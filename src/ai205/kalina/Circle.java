package ai205.kalina;

public class Circle {
    private double radius;
    private Point center;

    public Circle(Point center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public Point getCenter() {
        return center;
    }

    public boolean checkBelonging(Point point) {
        if(center.getDistance(point) <= radius) {
            return true;
        } else {
            return false;
        }
    }
}
