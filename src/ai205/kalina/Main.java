package ai205.kalina;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner console = new Scanner(System.in);
        System.out.println("Введите координаты точек:");
        System.out.println("Введите x:");
        int x = console.nextInt();
        System.out.println("Введите y:");
        int y = console.nextInt();
        System.out.println("x: " + x);
        System.out.println("y: " + y);
        Point point = new Point(x, y);
        System.out.println("Введите координату центра окружности x0:");
        int x0 = console.nextInt();
        System.out.println("Введите координату центра окружности y0:");
        int y0 = console.nextInt();
        System.out.println("x0: " + x0);
        System.out.println("y0: " + y0);
        Point center = new Point(x0, y0);
        System.out.println("Введите радиус окружности:");
        double radius = console.nextDouble();
        System.out.println("Радиус окружности: " + radius);
        Circle circle = new Circle(center, radius);
        if (circle.checkBelonging(point)) {
            System.out.println("Точка принадлежит окружности.");
        } else  System.out.println("Точка не принадлежит окружности.");
    }
}
