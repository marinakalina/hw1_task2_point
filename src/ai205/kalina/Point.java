package ai205.kalina;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Point {
    private int x;
    private int y;
    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public double getDistance(Point point){
        double distance;
        distance = sqrt(pow(this.x - point.getX(),2) + pow(this.y - point.getY(),2));
        return distance;
    }
}
